provider "google" {
  credentials = var.creds
  project = var.project
  region  = var.region
  zone    = var.zone
}
module "mcserver" {
  source  = "app.terraform.io/highmark-training/mcserver/google"
  version = "0.0.3"
  zone = var.zone
  name = var.name
  image = var.image
  identity = var.identity
  num_webs = var.num_webs
  network = var.network
}