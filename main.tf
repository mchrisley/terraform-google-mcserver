resource "google_compute_network" "vpc_network" {
  name = var.network
}

resource "google_compute_instance" "web" {
  count        = var.num_webs
  name         = "${var.name}-${count.index + 1}"
  machine_type = "f1-micro"
  zone         = var.zone

  boot_disk {
    initialize_params {
      image = var.image
    }
  }
  network_interface {
    network = google_compute_network.vpc_network.name

    access_config {

    }
  }
  tags = [var.identity,"yourname","env1"]
}